# Éxecuter les fichiers shell (.sh)

Il est possible d'exécuter les fichiers `.sh` sur Linux en utilisant la commande `sh`, du même nom.

Pour cela il suffit de faire :
```bash
sh fichier.sh
```

:rotating_light: Attention cependant, faites bien attention à ce que vous éxecutez !
